---
name: Tour of ITP
---

The local team will provide a tour of our venue (ITP Prizren). We will depart
from the main room right after the opening ceremony.
