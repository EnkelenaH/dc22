'use strict';

var cleanCSS = require('gulp-clean-css');
var concat = require('gulp-concat');
var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');

function css() {
  return gulp.src([
      'assets/scss/main.scss',
      'assets/scss/badges.scss',
    ])
    .pipe(sourcemaps.init())
    .pipe(
      sass({
        includePaths: 'node_modules',
      })
      .on('error', sass.logError)
    )
    .pipe(cleanCSS())
    .pipe(sourcemaps.write('../maps'))
    .pipe(gulp.dest('static/css/'));
}
exports.css = css;

function js() {
  return gulp.src([
      'node_modules/jquery/dist/jquery.js',
      'node_modules/popper.js/dist/umd/popper.js',
      'node_modules/bootstrap/dist/js/bootstrap.js',
      'node_modules/moment/moment.js',
      'node_modules/moment/locale/en-gb.js',
      'node_modules/moment-timezone/builds/moment-timezone-with-data.js',
      'node_modules/eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js',
      'node_modules/video.js/dist/video.js',
      'node_modules/video.js/dist/lang/en.js',
      'node_modules/videojs-contrib-quality-levels/dist/videojs-contrib-quality-levels.js',
      'node_modules/videojs-hls-quality-selector/dist/videojs-hls-quality-selector.js',
      'assets/js/reg-form-dependency.js',
    ])
    .pipe(sourcemaps.init())
    .pipe(concat('debconf22.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write('../maps'))
    .pipe(gulp.dest('static/js'));
}
exports.js = js;

function vendor_js() {
  return gulp.src('node_modules/jquery/dist/jquery.js')
    .pipe(sourcemaps.init())
    .pipe(uglify())
    .pipe(sourcemaps.write('../maps'))
    .pipe(gulp.dest('static/js'));
}
exports.vendor_js = vendor_js;

function assets() {
  return gulp.src('assets/{img,fonts,docs,video}/**/*')
    .pipe(gulp.dest('static/'));
}
exports.assets = assets;

function font_awesome() {
  return gulp.src('node_modules/@fortawesome/fontawesome-free-webfonts/webfonts/*')
    .pipe(gulp.dest('static/fonts'));
}
exports.font_awesome = font_awesome;

function watch() {
  gulp.watch('assets/js/*.js', js);
  gulp.watch('assets/scss/*.scss', css);
  gulp.watch('assets/img/*', assets);
  gulp.watch('assets/fonts/*', assets);
}
exports.watch = watch;

exports.default = gulp.series(
  css,
  js,
  vendor_js,
  assets,
  font_awesome,
);
