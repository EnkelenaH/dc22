---
title: Welcome to DebConf22
---

Welcome to DebConf22, hosted at the
[ITP - Innovation and Training Park Prizren](https://itp-prizren.com) in Kosovo.

Please read the contents of this page and the
[Conference Venue page](https://debconf22.debconf.org/about/venue) carefully.
They contain important information about your travel to Prizren, and DebConf22.

## Before departing for Prizren

Print this webapge and the relevant information in the web site (e.g. a [venue
map](https://debconf22.debconf.org/static/docs/venue-map-22.pdf)).

Check your data [here](https://debconf22.debconf.org/register/step-2) and
[here](https://debconf22.debconf.org/register/step-7), and report any errors and
changes (including delays or cancellation) as soon as possible to
<registration@debconf.org>

Familiarize yourself with the Codes of Conduct for both
[Debian](http://debconf.org/codeofconduct.shtml) and
[DebConf](https://www.debian.org/code_of_conduct).  If you are a victim of
harassment, or observe it, you can contact <community@debian.org> or members of
the on-site [anti-harassment team](https://debconf22.debconf.org/about/coc/)
who are recognizable by their conference badges.

For any other kind of problem, the Front Desk and Organisation team may be of
help.

Check your travel documents.  Will your passport remain valid for the duration
of your stay?  Do you have all necessary travel authorizations (such as a
visa)?

Notify your bank that you'll be travelling to Kosovo to avoid having your
card(s) blocked.

Do you have travel insurance? Be sure to also check with your insurance if
you're covered for lost or stolen items, such as electronics.

Take a COVID-19 self-test, if you can get your hands on one. You wouldn't want
to infect other people at the conference or on the plane.

Cheese & Wine allowance:

- Up to 2kg of cheese may be imported per person.
- Up to 2l of wine OR 1l of spirits may be imported per person.

## On-site Accommodation

Check-in will be done at Front Desk, at the entrance to Drini, the main talk
room in the concrete block.  You just need to go there and give your name.
They will know which room is assigned to you.

If you're arriving later than 18:00, you need to contact Front Desk at least
24h in advance and pre-arrange check-in, otherwise no one will be available to
give you your keys.  The Front Desk phone number is below.

The venue provides basic bedding (bed sheets, blanket, pillow).  The rooms
contain 2 beds, a desk, a closet, and air conditioning.

You will need to bring personal toiletries, such as toothbrush, toothpaste,
soap, shampoo, towels, and (optionally) slippers.  A laundry service will be
offered.

Most on-site attendees are sharing a room with others, so if you're sensitive
to noise when sleeping, consider bringing earplugs.

Read more about accommodation
[here](https://debconf22.debconf.org/about/accommodation)

## What to bring

An international adapter (if required) and power supplies for all your devices.
Kosovo uses European Type-F sockets at 220V.

Summer clothes.  It's hot in Kosovo, but it's also the rainy season.  The
temperature is expected to range from 10°C to 35°C.  It may rain all day, so a
raincoat and/or umbrella will be useful.

Don't forget sunscreen if you plan to enjoy the outdoors.

Printed PGP/GPG fingerprint slips, if you want to have your key signed by your
peers.

## Arrival in Prizren

You can walk to the venue from the centre of Prizren, it's about 2.5km, or take
a taxi.

The main entrance to ITP is a large white security gate followed by a guard and
booms.  You can drive in, up to the venue buildings.  Tell your taxi driver
that it's OK to drive in.  If the gate is closed after hours, get the guard's
attention by honking / calling out.

### Taxis

Taxi from Prishtina International Airport to Prizren costs €55.
Taxi from Prishtina International Airport to Prishtina costs €15

Taxi companies work 24/7. Taxis are quite inexpensive: you can get around the
city for around €3.5. Starting price is €2.

Local names for the venue (ITP):

- “Te Kampi Gjerman” (pronounced Te Kompi German)
- "Ish Kazerma"
- "Ish kampi i Kforit" (Ish Kompi Ik Forit).

Taxi companies serving Prizren:

- Radio Taxi Fortuna: +383 49 380 000, +383 44 380 000, (Viber/Whatsapp)
- Taxi OK: +383 49 333 500, +383 44 333 500, (Viber/Whatsapp)
- Blue Taxi: +383 49 555 252, +383 44 711 711, (Viber)

## Prishtina Airport Arrival

You can buy local SIM cards at the airport (look for the IPKO store).

There is a public transport bus line from Prishtina International Airport to
the Bus Station in Prishtina.
<http://www.limakkosovo.aero/public-transport>

Take the hourly airport shuttle bus (Line 1A) to Prishtina Bus Station (€3).

Bus lines from Prishtina Bus Station to Prizren travel every 20 minutes (€5).
The bus passes by the venue (ask to be dropped at Kampi Gjerman i KFOR-it).

If you end up at Prizren Bus Station or any other place around Prizren, taxis
to the venue cost €2 - €2.5.

## Tirana Airport Arrival

Tirana International Airport is 2:10 hrs by bus (163 km) away. Tirana -
Prishtina bus stops by Tirana International Airport with a stopover in the
periphery of Prizren, from which you may call a taxi.
It costs €15 one way.

## Skopje Airport Arrival

Your route will be: Airport shuttle to Skopje Bus Station (300 denar or €3),
from there, a few direct buses go to Prizren. Skopje - Prishtina (€7) is
more readily available and from there to Prizren every 20 minutes (€4). It
will take 3-4 hrs altogether if connections work out optimally.

## Kukës Airport Arrival

Get to the town of Kukës, from there vans (furgon) go regularly to Prizren.

## Front Desk location

The Front Desk is located in the same concrete block building as Drini, the
main talk room. Enter to the left of the "ITP > Prizren" sign. It is open from
10:00 to 18:00 daily.

If you run into any problems, you can call the DebConf 22 Front Desk at +383 49
284 591 (also on Signal).

Save this in your phone now.

Have a safe trip.  We look forward to seeing you in Prizren!

The DebConf Team
